# README #

Foodspring,  das gesunde Fitness Food für zuhause und unterwegs.
Mit der Sportnahrung von Foodspring kannst Du Muskeln aufbauen, abnehmen und dich dabei sogar einfach nur gesund ernähren. Nach dem Workout kannst Du Dir einen eiweißreichen Protein Shake mixen. Einen leckeren Shape Shake für den Heißhunger bietet Foodspring Dir ebenfalls an. So bist Du optimal auf Dein Training vorbereitet.

Nahrungsergänzungsprodukte
Foodpring stellt hochwertige Sportnahrung her. Das Sortiment der Firma wird ständig um neue Produkte erweitert. 
Beispiele Foodspring Sortiment 2019
Molkeprotein
Shape Shake
Bio-Protein (aus Bio-Milchprotein)
Vegan Protein (100% pflanzliches Protein)
Protein-Haferflocken 
Kreatinkapseln und Pulver
L-Glutamin
L-Carnitin
Cholin-Inositol
Bio-Kokosöl
Geformte Dächer
Tägliche Vitamine
Superfood Grüne und Beeren, Nüsse und Beeren, Bio Goji Beeren
Biologische weiße Chiasamen 
Auf https://younalize.com/foodspring/ findest alle Produkte aus den Shop. Der Shop ist sehr gut sortiert sodass man das gewünschte Produkt schnell findet. Bei Foodspring gibt es Produkte für Muskelaufbau, Gewichtsverlust, Ausdauer oder auch die Gesundheit. 
Wenn Du über bestimmte Produktemehr erfahren willst und weitere Infos benötigst, kannst Du Dich per E-Mail oder Telefon an den Foodspring Kundendienst wenden. Auch wenn Du Fragen zu Deiner Bestellung hast kannst Du dich an den Kundenservice wenden. 
Foodpring Bodycheck
Wenn man nicht viel Erfahrung mit Sportnahrung hat, hilft einem der kostenlose Bodycheck weiter. Man bekommt eine Produktempfehlung, die auf den eigenen Daten und Zielen basiert. Unter anderem werden Daten über Ihr Ziel (Muskelaufbau, Verbesserung der Ausdauer, Gewichtsabnahme oder Gesundheit), Ihr Alter, Ihre Größe, Ihr Gewicht und Ihr Geschlecht abgerufen. Im Rahmen des Körperchecks werden Fragen zu Ihrem Lebensstil und Ihren Sporteinheiten sowie Ihrer Ernährung gestellt. Hier finden Sie eine kurze Auswahl Ihrer persönlichen Daten.

